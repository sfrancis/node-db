import 'reflect-metadata';

import oracledb from 'oracledb';

import { oracleDatabaseTypes } from './enums';
import { formatQuery, formatStoredProcedureQuery, getConnection, MapUtils } from './helpers';
import consumeStream from './helpers/consume-stream';
import { BindParameters } from './types';

export class OracleOperations {
  constructor() {
    oracledb.fetchAsString = [oracledb.CLOB, oracledb.DATE];
    oracledb.fetchAsBuffer = [oracledb.BLOB];
  }
  async executeStoredProcedure(procedure: string, inputParameters: BindParameters, databaseName: string): Promise<any> {
    const connection = await getConnection(databaseName);
    try {
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      const executeProcedureResult = await connection.execute(formatStoredProcedureQuery(procedure, bindParameters), bindParameters, {
        autoCommit: true,
      });
      return executeProcedureResult.rows;
    } finally {
      await connection.close();
    }
  }

  async deleteRecords(
    procedure: string,
    inputParameters: BindParameters,
    databaseName: string,
    outputParameter = 'OUT_RECORDCOUNT',
  ): Promise<any> {
    const connection = await getConnection(databaseName);
    try {
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      bindParameters[outputParameter] = {
        dir: oracledb.BIND_OUT,
        type: oracledb.DB_TYPE_NUMBER,
      };
      const formattedSQLStatement = formatStoredProcedureQuery(procedure, bindParameters);

      const updateProcedureResult = await connection.execute(formattedSQLStatement, bindParameters, { autoCommit: true });
      return updateProcedureResult.outBinds[outputParameter];
    } finally {
      await connection.close();
    }
  }

  async updateRecords(
    procedure: string,
    inputParameters: BindParameters,
    databaseName: string,
    outputParameter = 'OUT_RECORDCOUNT',
  ): Promise<any> {
    const connection = await getConnection(databaseName);
    try {
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      bindParameters[outputParameter] = {
        dir: oracledb.BIND_OUT,
        type: oracledb.DB_TYPE_NUMBER,
      };
      const formattedSQLStatement = formatStoredProcedureQuery(procedure, bindParameters);

      const updateProcedureResult = await connection.execute(formattedSQLStatement, bindParameters, { autoCommit: true });
      return updateProcedureResult.outBinds[outputParameter];
    } finally {
      await connection.close();
    }
  }

  async insertRecords(
    procedure: string,
    inputParameters: BindParameters,
    databaseName: string,
    outputParameter = 'OUT_RECORDCOUNT',
  ): Promise<any> {
    const connection = await getConnection(databaseName);
    try {
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      bindParameters[outputParameter] = {
        dir: oracledb.BIND_OUT,
        type: oracledb.DB_TYPE_NUMBER,
      };
      const formattedSQLStatement = formatStoredProcedureQuery(procedure, bindParameters);

      const updateProcedureResult = await connection.execute(formattedSQLStatement, bindParameters, { autoCommit: true });
      return updateProcedureResult.outBinds[outputParameter];
    } finally {
      await connection.close();
    }
  }

  async queryScalar(
    sqlStatement: string,
    inputParameters: BindParameters,
    returnType: oracleDatabaseTypes,
    databaseName: string,
  ): Promise<any> {
    let connection: oracledb.Connection;
    try {
      let dbType = oracledb.DB_TYPE_VARCHAR;
      switch (returnType) {
        case oracleDatabaseTypes.string:
          dbType = oracledb.DB_TYPE_VARCHAR;
          break;
        case oracleDatabaseTypes.number:
          dbType = oracledb.DB_TYPE_NUMBER;
          break;
        case oracleDatabaseTypes.date:
          dbType = oracledb.DB_TYPE_VARCHAR;
          break;
        case oracleDatabaseTypes.boolean:
          dbType = oracledb.DB_TYPE_NUMBER;
          break;
        case oracleDatabaseTypes.clob:
          dbType = oracledb.CLOB;
          break;
        default:
          dbType = oracledb.DB_TYPE_VARCHAR;
          break;
      }
      const outParam = 'out_param';
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });

      bindParameters[outParam] = {
        dir: oracledb.BIND_OUT,
        type: dbType,
      };

      connection = await getConnection(databaseName);
      const queryResult = await connection.execute(formatQuery(sqlStatement, bindParameters), bindParameters, {
        outFormat: oracledb.OUT_FORMAT_OBJECT,
      });
      return queryResult.outBinds[outParam];
    } finally {
      await connection.close();
    }
  }

  async queryMulti<T>(
    sqlStatement: string,
    inputParameters: BindParameters,
    outputParameters: Record<string, oracleDatabaseTypes>,
    returnType: new () => T,
    databaseName: string,
  ): Promise<any> {
    let connection: oracledb.Connection;
    try {
      const outParams = {};
      Object.keys(outputParameters).forEach((key) => {
        switch (outputParameters[key]) {
          case oracleDatabaseTypes.string:
            outParams[key] = {
              dir: oracledb.BIND_OUT,
              type: oracledb.DB_TYPE_VARCHAR,
            };
            break;
          case oracleDatabaseTypes.number:
            outParams[key] = {
              dir: oracledb.BIND_OUT,
              type: oracledb.DB_TYPE_NUMBER,
            };
            break;
          case oracleDatabaseTypes.date:
            outParams[key] = {
              dir: oracledb.BIND_OUT,
              type: oracledb.DB_TYPE_VARCHAR,
            };
            break;
          case oracleDatabaseTypes.boolean:
            outParams[key] = {
              dir: oracledb.BIND_OUT,
              type: oracledb.DB_TYPE_NUMBER,
            };
            break;
          case oracleDatabaseTypes.cursor:
            outParams[key] = {
              dir: oracledb.BIND_OUT,
              type: oracledb.CURSOR,
            };
            break;
          default:
            outParams[key] = {
              dir: oracledb.BIND_OUT,
              type: oracledb.DB_TYPE_VARCHAR,
            };
            break;
        }
      });
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString() || null;
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      Object.assign(bindParameters, outParams);
      connection = await getConnection(databaseName);
      const queryResult = await connection.execute(formatQuery(sqlStatement, bindParameters), bindParameters, {
        outFormat: oracledb.OUT_FORMAT_OBJECT,
        fetchArraySize: 1000000,
      });
      const result = new returnType();
      const resultKeys = Object.keys(result);
      const outBindKeys = Object.keys(queryResult.outBinds);
      for (const param of Object.keys(outParams)) {
        const outPrefix = 'out_';
        const outParam = outBindKeys.find((outBindKey) => outBindKey.toUpperCase() === param.toUpperCase());
        if (outParam) {
          const outParamTrimmed = outParam.toLowerCase().startsWith(outPrefix) ? outParam.slice(outPrefix.length) : outParam || '';
          const currentResultKey = resultKeys.find((resultKey) => resultKey.toUpperCase() === outParamTrimmed.toUpperCase());
          if (currentResultKey) {
            if (outParams[param].type === oracledb.CURSOR) {
              const cursorResult = await consumeStream(queryResult.outBinds[outParam].toQueryStream());
              const propertyType = Reflect.getMetadata('jsonProperty', result, currentResultKey);
              result[currentResultKey] = MapUtils.deserialize([propertyType.class], cursorResult);
            } else {
              result[currentResultKey] = queryResult.outBinds[outParam];
            }
          }
        }
      }
      return result as T;
    } finally {
      await connection.close();
    }
  }

  async queryArray<T>(
    sqlStatement: string,
    inputParameters: BindParameters,
    returnType: [new () => T],
    databaseName: string,
  ): Promise<T[]> {
    let connection: oracledb.Connection;
    try {
      const outCursor = 'out_cursor';
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      bindParameters[outCursor] = {
        dir: oracledb.BIND_OUT,
        type: oracledb.CURSOR,
      };
      connection = await getConnection(databaseName);
      const queryResult = await connection.execute(formatQuery(sqlStatement, bindParameters), bindParameters, {
        outFormat: oracledb.OUT_FORMAT_OBJECT,
        fetchArraySize: 1000000,
      });
      const cursor = queryResult.outBinds[outCursor];
      const result = await consumeStream(cursor.toQueryStream());
      const returnResult = MapUtils.deserialize(returnType, result);
      return returnResult as unknown as T[];
    } finally {
      if (connection) {
        await connection.close();
      }
    }
  }

  async queryFirstDefault<T>(
    sqlStatement: string,
    inputParameters: BindParameters,
    returnType: new () => T,
    databaseName: string,
  ): Promise<T> {
    let connection: oracledb.Connection;
    try {
      const outCursor = 'out_cursor';
      // const bindParameters = inputParameters;
      const bindParameters: oracledb.BindParameters = {};
      Object.keys(inputParameters).forEach((key: any) => {
        if (Array.isArray(inputParameters[key])) {
          bindParameters[key] = (inputParameters[key] || []).toString();
        } else {
          bindParameters[key] = inputParameters[key];
        }
      });
      bindParameters[outCursor] = {
        dir: oracledb.BIND_OUT,
        type: oracledb.CURSOR,
      };
      connection = await getConnection(databaseName);
      const queryResult = await connection.execute(formatQuery(sqlStatement, bindParameters), bindParameters, {
        outFormat: oracledb.OUT_FORMAT_OBJECT,
      });

      const cursor = queryResult.outBinds[outCursor];
      const result = await consumeStream(cursor.toQueryStream());
      const returnResult = MapUtils.deserialize(returnType, result[0]);

      return returnResult;
    } finally {
      if (connection) {
        await connection.close();
      }
    }
  }
}
