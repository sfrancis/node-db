import oracledb from 'oracledb';

import { OracleSettings } from '../interfaces';

const generateConnectionString = (dbConfig: OracleSettings): string => {
  return dbConfig.host + ':' + dbConfig.port + '/' + dbConfig.service;
};

export const getConnection = async (database: string): Promise<oracledb.Connection> => {
  const connection = await oracledb.getConnection({ poolAlias: database });
  return connection;
};

export const createPool = async (dbConfig: OracleSettings, poolAlias: string): Promise<void> => {
  if (dbConfig) {
    await oracledb.createPool({
      user: dbConfig.user,
      password: dbConfig.password,
      connectString: generateConnectionString(dbConfig),
      poolAlias,
    });
  } else {
    throw new Error(`${poolAlias} - Database not Configured`);
  }
};

export const closePool = async (database: string): Promise<void> => {
  const pool = oracledb.getPool(database);
  if (pool) {
    await pool.close();
  }
};
