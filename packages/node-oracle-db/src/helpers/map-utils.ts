/* istanbul ignore next */
import 'reflect-metadata';

export class MapUtils {
  static isPrimitive(obj: object): boolean {
    switch (typeof obj) {
      case 'string':
      case 'number':
      case 'boolean':
        return true;
    }
    return !!(
      obj instanceof String ||
      obj === String ||
      obj instanceof Number ||
      obj === Number ||
      obj instanceof Boolean ||
      obj === Boolean
    );
  }

  // tslint:disable-next-line: no-untyped-public-signature
  static isArray(object) {
    if (object === Array) {
      return true;
    } else if (typeof Array.isArray === 'function') {
      return Array.isArray(object);
    } else {
      return !!(object instanceof Array);
    }
  }

  // tslint:disable-next-line: no-untyped-public-signature
  static getClazz(target: any, propertyKey: string): any {
    return Reflect.getMetadata('design:type', target, propertyKey);
  }

  // tslint:disable-next-line: no-untyped-public-signature
  static getJsonProperty(target: any, propertyKey: string) {
    return Reflect.getMetadata('jsonProperty', target, propertyKey);
  }

  // tslint:disable-next-line: no-untyped-public-signature
  static deserialize(clazz: any, json: any): any {
    if (MapUtils.isArray(json)) {
      return MapUtils.deserializeJsonArray(clazz, json);
    } else {
      return MapUtils.deserializeObject(clazz, json);
    }
  }

  private static deserializeObject<T>(clazz: new () => T, jsonObject) {
    if (clazz === undefined || jsonObject === undefined) {
      return undefined;
    }
    const obj = new clazz();
    Object.keys(obj).forEach((key) => {
      const propertyMetadataFn: (arg: any) => any = (propertydata) => {
        const propertyName = propertydata.name || key.toUpperCase();
        const innerJson = jsonObject ? jsonObject[propertyName] : undefined;
        if (MapUtils.isArray(clazz)) {
          const metadata = this.getJsonProperty(obj, key);
          if (metadata.class || MapUtils.isPrimitive(clazz)) {
            if (innerJson && MapUtils.isArray(innerJson)) {
              return innerJson.map((item) => MapUtils.deserialize(metadata.class, item));
            } else {
              return undefined;
            }
          } else {
            return innerJson;
          }
        } else if (!MapUtils.isPrimitive(clazz)) {
          return MapUtils.deserialize(clazz, innerJson);
        } else {
          return jsonObject ? jsonObject[propertyName] : undefined;
        }
      };

      const propertyMetadata = this.getJsonProperty(obj, key);
      if (propertyMetadata) {
        obj[key] = propertyMetadataFn(propertyMetadata);
      } else {
        if (
          jsonObject &&
          (jsonObject[key] || jsonObject[key.toUpperCase()] === 0 || jsonObject[key.toUpperCase()] || jsonObject[key.toLowerCase()]) !==
            undefined
        ) {
          obj[key] =
            jsonObject[key.toUpperCase()] === 0 ? 0 : jsonObject[key] || jsonObject[key.toUpperCase()] || jsonObject[key.toLowerCase()];
        }
      }
    });
    return obj;
  }

  private static deserializeJsonArray<T>(clazz: [new () => T], jsonArray) {
    if (clazz === undefined || jsonArray === undefined) {
      return undefined;
    }
    const objArray = [];
    jsonArray.forEach((jsonObject) => {
      objArray.push(MapUtils.deserialize(clazz[0], jsonObject));
    });
    return objArray;
  }
}
