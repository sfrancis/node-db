import oracledb from 'oracledb';

import { BindParameters } from '../types';
import { getConnection } from './connection';
import consumeStream from './consume-stream';
import { formatQuery } from './format-query';

export const queryStream = async <T>(
  sqlStatement: string,
  inputParameters: BindParameters,
  databaseName: string,
  outCursor?: string,
): Promise<T> => {
  let connection: oracledb.Connection;
  try {
    const bindParameters: oracledb.BindParameters = {};
    Object.keys(inputParameters).forEach((key: any) => {
      if (Array.isArray(inputParameters[key])) {
        bindParameters[key] = (inputParameters[key] || []).toString();
      } else {
        bindParameters[key] = inputParameters[key];
      }
    });
    connection = await getConnection(databaseName);

    const stream = await connection.queryStream(formatQuery(sqlStatement, bindParameters), bindParameters, { fetchArraySize: 150 });
    const resultSet = await consumeStream<T>(stream);
    return resultSet as unknown as T;
  } finally {
    if (connection) {
      await connection.close();
    }
  }
};
