const consumeStream = async <T>(queryStream: any): Promise<T[]> => {
  const result = [];
  const consumerStream = new Promise((resolve, reject) => {
    queryStream.on('data', (row) => {
      result.push(row);
    });
    queryStream.on('error', (error) => {
      reject(error);
    });
    queryStream.on('metadata', (metadata) => {
      // implement logic to handle meta data if required
    });
    queryStream.on('close', resolve);
  });
  await consumerStream;
  return result as T[];
};

export default consumeStream;
