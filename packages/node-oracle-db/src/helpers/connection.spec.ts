import oracledb = require('oracledb');

import { closePool, createPool, getConnection } from './connection';

const configMock = {
  test: {
    user: 'test',
    password: 'test',
    host: 'test',
    port: 1,
    service: 'test',
  },
};
jest.mock('oracledb', () => {
  const mockConnection = {
    execute: jest.fn(),
    queryStream: jest.fn(),
    release: jest.fn(),
    close: jest.fn(),
  };
  const mockPool = {
    getConnection: jest.fn(() => mockConnection),
    close: jest.fn(() => true),
  };
  return {
    getPool: jest.fn(() => mockPool),
    createPool: jest.fn(() => mockPool),
    getConnection: jest.fn(() => mockConnection),
    BIND_OUT: 10,
    DB_TYPE_VARCHAR: 10,
  };
});
let pool: oracledb.Pool;
let pooledConnection: oracledb.Connection;
describe('OracleServiceTest', () => {
  beforeEach(async () => {
    pool = await oracledb.createPool({
      user: 'test',
      password: 'test',
      connectString: 'test',
      poolAlias: 'test',
    });
    pooledConnection = await pool.getConnection();
  });

  it('createPool success', async () => {
    try {
      await createPool(configMock.test, 'test');
    } catch (error) {
      expect(error).toBeUndefined();
    }
  });

  it('getConnection success', async () => {
    try {
      const connection = await getConnection('test');
      expect(connection).toBeDefined();
    } catch (error) {
      expect(error).toBeUndefined();
    }
  });

  it('closePool success', async () => {
    try {
      await closePool('test');
    } catch (error) {
      expect(error).toBeUndefined();
    }
  });
});
