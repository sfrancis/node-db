import oracledb from 'oracledb';

export const formatQuery = (sqlStatement: string, bindParameters: oracledb.BindParameters): string => {
  let bindParameterString = ``;
  const paramKeys = Object.keys(bindParameters);
  if (paramKeys.length === 0) {
    return sqlStatement;
  } else {
    paramKeys.forEach((key) => {
      bindParameterString = bindParameterString.concat(paramKeys.indexOf(key) === paramKeys.length - 1 ? `:${key}` : `:${key}, `);
    });

    return `BEGIN
    ${sqlStatement}(${bindParameterString});
    END;`;
  }
};

export const formatStoredProcedureQuery = (procedure: string, bindParameters: oracledb.BindParameters): string => {
  let bindParameterString = ``;
  const paramKeys = Object.keys(bindParameters);
  paramKeys.forEach((key) => {
    bindParameterString = bindParameterString.concat(paramKeys.indexOf(key) === paramKeys.length - 1 ? `:${key}` : `:${key}, `);
  });
  const tempValue = `BEGIN
    ${procedure}(${bindParameterString});
    END;`;
  return tempValue;
};
