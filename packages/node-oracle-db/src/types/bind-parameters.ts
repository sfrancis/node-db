import oracledb from 'oracledb';

export type BindParameters =
  | Record<string, oracledb.BindParameter | string | number | Date | oracledb.DBObject_IN<any> | string[] | Buffer | null | undefined>
  | oracledb.BindParameter[]
  | any[];
