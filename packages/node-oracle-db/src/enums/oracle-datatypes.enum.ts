export enum oracleDatabaseTypes {
  string = 1,
  number,
  boolean,
  date,
  cursor,
  clob,
}
