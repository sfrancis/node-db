export interface OracleSettings {
  user: string;
  password: string;
  host: string;
  port: number;
  service: string;
}
