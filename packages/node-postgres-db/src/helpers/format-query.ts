export const formatQuery = (functionName: string, parameters: any[]): string => {
  let formattedQuery = `SELECT * FROM ${functionName}(`;
  parameters.forEach((parameter) => {
    const typeCheck = (param: any): string => {
      if (typeof param === 'string') {
        return `'${param}'`;
      } else if (param && Array.isArray(param)) {
        if (param.length > 0) {
          return `'{${param.toString()}}'`;
        } else {
          return null;
        }
      } else if (param === null || param === undefined) {
        return null;
      } else {
        return param;
      }
    };

    formattedQuery = formattedQuery.concat(`${typeCheck(parameter)}, `);
  });
  formattedQuery = formattedQuery.trim();
  formattedQuery = formattedQuery[formattedQuery.length - 1] === ',' ? formattedQuery.slice(0, -1).concat(')') : formattedQuery.concat(')');

  return formattedQuery;
};
