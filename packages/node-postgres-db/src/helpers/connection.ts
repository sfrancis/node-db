import { Pool } from 'pg';

import { PostgresSettings } from '../interfaces';

export const createPool = async (dbConfig: PostgresSettings): Promise<Pool> => {
  if (dbConfig) {
    const connectionPool = new Pool({
      user: dbConfig.user,
      host: dbConfig.host,
      database: dbConfig.database,
      password: dbConfig.password,
      port: dbConfig.port,
    });
    return connectionPool;
  } else {
    throw new Error(`Database not Configured`);
  }
};

export const closePools = async (connectionPools: { [key: string]: Pool }): Promise<void> => {
  try {
    for (const key in connectionPools) {
      if (connectionPools[key]) {
        connectionPools[key]?.end();
      }
    }
  } catch (error) {
    throw new Error(`Pool Close failed`);
  }
};
