export interface ExceuteFunctionArgs {
  functionName: string;
  parameters?: any[];
  database?: string;
  schema?: string;
}
