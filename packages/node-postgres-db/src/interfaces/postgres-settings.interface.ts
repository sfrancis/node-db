export interface PostgresSettings {
  user: string;
  password: string;
  host: string;
  port: number;
  database: string;
  searchPath: string;
}
