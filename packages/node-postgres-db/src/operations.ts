import 'reflect-metadata';

import { Pool, PoolClient } from 'pg';
import QueryStream from 'pg-query-stream';

import { createPool, formatQuery, MapUtils } from './helpers';
import { closePools } from './helpers/connection';
import { ExceuteFunctionArgs } from './interfaces';
import { PostgresSettings } from './interfaces/postgres-settings.interface';

export class PostgresOperations {
  private readonly connectionPools: { [key: string]: Pool } = {};
  constructor() {
    this.connectionPools = {};
  }
  private async getPoolClient(database: string): Promise<PoolClient> {
    const connectionPool = this.connectionPools[database];
    return connectionPool.connect();
  }

  private addToPool(key: string, conectionPool: Pool) {
    this.connectionPools[key] = conectionPool;
  }

  async closeConnectionPools(): Promise<void> {
    closePools(this.connectionPools);
  }

  async createConnectionPools(dbConfigs: { [key: string]: PostgresSettings }): Promise<void> {
    for (const database in dbConfigs) {
      if (Object.prototype.hasOwnProperty.call(dbConfigs, database)) {
        this.addToPool(database, await createPool(dbConfigs[database]));
        const client = await this.getPoolClient(database);
        await client.query(`SET search_path TO '${dbConfigs[database].searchPath}';`);
      }
    }
  }

  async insertRecords<T>(executeFunctionArgs: ExceuteFunctionArgs): Promise<T> {
    let client: PoolClient;
    const result = [];
    try {
      client = await this.getPoolClient(executeFunctionArgs.database);
      const formattedQuery = formatQuery(executeFunctionArgs.functionName, executeFunctionArgs.parameters);
      const queryStream = await client.query(new QueryStream(formattedQuery));
      const consumeStream = new Promise((resolve, reject) => {
        queryStream.on('data', (row: T) => {
          result.push(row);
        });
        queryStream.on('error', (error) => {
          reject(error);
        });
        queryStream.on('end', resolve);
      });
      await consumeStream;
      const returnResult = result[0][executeFunctionArgs.functionName];
      return returnResult;
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async updateRecords<T>(executeFunctionArgs: ExceuteFunctionArgs): Promise<T> {
    let client: PoolClient;
    const result = [];
    try {
      client = await this.getPoolClient(executeFunctionArgs.database);
      const formattedQuery = formatQuery(executeFunctionArgs.functionName, executeFunctionArgs.parameters);
      const queryStream = await client.query(new QueryStream(formattedQuery));
      const consumeStream = new Promise((resolve, reject) => {
        queryStream.on('data', (row: T) => {
          result.push(row);
        });
        queryStream.on('error', (error) => {
          reject(error);
        });
        queryStream.on('end', resolve);
      });
      await consumeStream;
      const returnResult = result[0][executeFunctionArgs.functionName];
      return returnResult;
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async deleteRecords<T>(executeFunctionArgs: ExceuteFunctionArgs): Promise<T> {
    let client: PoolClient;
    const result = [];
    try {
      client = await this.getPoolClient(executeFunctionArgs.database);
      const formattedQuery = formatQuery(executeFunctionArgs.functionName, executeFunctionArgs.parameters);
      const queryStream = await client.query(new QueryStream(formattedQuery));
      const consumeStream = new Promise((resolve, reject) => {
        queryStream.on('data', (row: T) => {
          result.push(row);
        });
        queryStream.on('error', (error) => {
          reject(error);
        });
        queryStream.on('end', resolve);
      });
      await consumeStream;
      const returnResult = result[0][executeFunctionArgs.functionName];

      return returnResult;
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async queryFirstDefault<T>(executeFunctionArgs: ExceuteFunctionArgs, returnType: new () => T): Promise<T> {
    let client: PoolClient;
    const result = [];
    try {
      client = await this.getPoolClient(executeFunctionArgs.database);
      const formattedQuery = formatQuery(executeFunctionArgs.functionName, executeFunctionArgs.parameters);
      const queryStream = await client.query(new QueryStream(formattedQuery));
      const consumeStream = new Promise((resolve, reject) => {
        queryStream.on('data', (row: T) => {
          result.push(row);
        });
        queryStream.on('error', (error) => {
          reject(error);
        });
        queryStream.on('end', resolve);
      });
      await consumeStream;
      const returnResult = MapUtils.deserialize(returnType, result[0]);

      return returnResult;
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async queryScalar(executeFunctionArgs: ExceuteFunctionArgs): Promise<any> {
    let client: PoolClient;
    try {
      client = await this.getPoolClient(executeFunctionArgs.database);
      const formattedQuery = formatQuery(executeFunctionArgs.functionName, executeFunctionArgs.parameters);
      const queryStream = await client.query(formattedQuery);

      return queryStream.rows[0][executeFunctionArgs.functionName];
    } finally {
      if (client) {
        client.release();
      }
    }
  }

  async queryArray<T>(executeFunctionArgs: ExceuteFunctionArgs, returnType: [new () => T]): Promise<T[]> {
    let client: PoolClient;
    const result = [];
    try {
      client = await this.getPoolClient(executeFunctionArgs.database);
      const formattedQuery = formatQuery(executeFunctionArgs.functionName, executeFunctionArgs.parameters);
      const queryStream = await client.query(new QueryStream(formattedQuery));
      const consumeStream = new Promise((resolve, reject) => {
        queryStream.on('data', (row: T) => {
          result.push(row);
        });
        queryStream.on('error', (error) => {
          reject(error);
        });
        queryStream.on('end', resolve);
      });
      await consumeStream;
      const returnResult = MapUtils.deserialize(returnType, result);

      return returnResult;
    } finally {
      if (client) {
        client.release();
      }
    }
  }
}
