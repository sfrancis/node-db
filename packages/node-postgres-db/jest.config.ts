import baseConfig from '../../jest.config.base';

const config = {
  ...baseConfig,
  rootDir: '.',
  displayName: 'Node Postgres',
  roots: ['<rootDir>'],
};
export default config;