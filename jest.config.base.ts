const baseConfig = {
  moduleFileExtensions: ['js', 'json', 'ts'],
  setupFiles: ['../../jest-setup-file.ts'],
  testRegex: '^.*\\.(spec.ts)$',
  transform: {
    '^.+\\.(t|j)s$': 'ts-jest',
  },
  coverageDirectory: './coverage',
  collectCoverage: true,
  testEnvironment: 'node',
  coveragePathIgnorePatterns: ['/test/', '/node_modules/'],
  collectCoverageFrom: ['**/*.ts', '!**/*/index.ts'],
  coverageReporters: ['json', 'lcov', 'text-summary', 'clover'],
};
export default baseConfig;
